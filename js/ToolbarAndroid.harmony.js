/*
 * Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
 * Use of this source code is governed by a MIT license that can be
 * found in the LICENSE file.
 */

'use strict';

const React = require('react');
const UIManager = require('react-native/Libraries/ReactNative/UIManager');

const resolveAssetSource = require('react-native/Libraries/Image/resolveAssetSource');

import ToolbarAndroidNativeComponent from './specs/index'
import type {SyntheticEvent} from 'react-native/Libraries/Types/CoreEventTypes';
import type {ImageSource} from 'react-native/Libraries/Image/ImageSource';
import type {ColorValue} from 'react-native/Libraries/StyleSheet/StyleSheetTypes';
import type {ViewProps} from 'react-native/Libraries/Components/View/ViewPropTypes';
import type {NativeComponent} from 'react-native/Libraries/Renderer/shims/ReactNative';


type Action = $ReadOnly<{|
  title: string,
  icon?: ?ImageSource,
  show?: 'always' | 'ifRoom' | 'never',
  showWithText?: boolean,
|}>;

type ToolbarAndroidChangeEvent = SyntheticEvent<
  $ReadOnly<{|
    position: number,
  |}>,
>;

type ToolbarAndroidProps = $ReadOnly<{|
  ...ViewProps,
  /**
   * or text on the right side of the widget. If they don't fit they are placed in an 'overflow'
   * Sets possible actions on the toolbar as part of the action menu. These are displayed as icons
   * menu.
   *
   * This property takes an array of objects, where each object has the following keys:
   *
   * * `title`: **required**, the title of this action
   * * `icon`: the icon for this action, e.g. `require('./some_icon.png')`
   * * `show`: when to show this action as an icon or hide it in the overflow menu: `always`,
   * `ifRoom` or `never`
   * * `showWithText`: boolean, whether to show text alongside the icon or not
   */
  actions?: ?Array<Action>,
  /**
   * Sets the toolbar logo.
   */
  logo?: ?ImageSource,
  /**
   * Sets the navigation icon.
   */
  navIcon?: ?ImageSource,
  /**
   * Callback that is called when an action is selected. The only argument that is passed to the
   * callback is the position of the action in the actions array.
   */
  onActionSelected?: ?(position: number) => void,
  /**
   * Callback called when the icon is selected.
   */
  onIconClicked?: ?() => void,
  /**
   * Sets the overflow icon.
   */
  overflowIcon?: ?ImageSource,
  /**
   * Sets the toolbar subtitle.
   */
  subtitle?: ?string,
  /**
   * Sets the toolbar subtitle color.
   */
  subtitleColor?: ?ColorValue,
  /**
   * Sets the toolbar title.
   */
  title?: ?Stringish,
  /**
   * Sets the toolbar title color.
   */
  titleColor?: ?ColorValue,
  /**
   * Sets the content inset for the toolbar starting edge.
   *
   * The content inset affects the valid area for Toolbar content other than
   * the navigation button and menu. Insets define the minimum margin for
   * these components and can be used to effectively align Toolbar content
   * along well-known gridlines.
   */
  contentInsetStart?: ?number,
  /**
   * Sets the content inset for the toolbar ending edge.
   *
   * The content inset affects the valid area for Toolbar content other than
   * the navigation button and menu. Insets define the minimum margin for
   * these components and can be used to effectively align Toolbar content
   * along well-known gridlines.
   */
  contentInsetEnd?: ?number,
  /**
   * Used to set the toolbar direction to RTL.
   * In addition to this property you need to add
   */
  rtl?: ?boolean,
  /**
   * Used to locate this view in end-to-end tests.
   */
  testID?: ?string,
|}>;

type Props = $ReadOnly<{|
  ...ToolbarAndroidProps,
  forwardedRef: ?React.Ref<typeof ToolbarAndroidNativeComponent>,
|}>;

class ToolbarAndroid extends React.Component<Props> {
  _onSelect = (event: ToolbarAndroidChangeEvent) => {
    const position = event.nativeEvent.position;
    if (position === -1) {
      this.props.onIconClicked && this.props.onIconClicked();
    } else {
      this.props.onActionSelected && this.props.onActionSelected(position);
    }
  };

  render() {
    const {
      onIconClicked,
      onActionSelected,
      forwardedRef,
      ...otherProps
    } = this.props;

    const nativeProps: {...typeof otherProps, nativeActions?: Array<Action>} = {
      ...otherProps,
    };

    if (this.props.logo) {
      nativeProps.logo = resolveAssetSource(this.props.logo);
    }

    if (this.props.navIcon) {
      nativeProps.navIcon = resolveAssetSource(this.props.navIcon);
    }

    if (this.props.overflowIcon) {
      nativeProps.overflowIcon = resolveAssetSource(this.props.overflowIcon);
    }

    if (this.props.actions) {
      const nativeActions = [];
      for (let i = 0; i < this.props.actions.length; i++) {
        const action = {
          icon: this.props.actions[i].icon,
          show: this.props.actions[i].show,
        };

        if (action.icon) {
          action.icon = resolveAssetSource(action.icon);
        }

        nativeActions.push({
          ...this.props.actions[i],
          ...action,
        });
      }

      nativeProps.nativeActions = nativeActions;
    }

    return (
      <ToolbarAndroidNativeComponent
        onSelect={this._onSelect}
        {...nativeProps}
        ref={forwardedRef}
      />
    );
  }
}

const ToolbarAndroidToExport = React.forwardRef(
  (
    props: ToolbarAndroidProps,
    forwardedRef: ?React.Ref<typeof ToolbarAndroidNativeComponent>,
  ) => {
    return <ToolbarAndroid {...props} forwardedRef={forwardedRef} />;
  },
);

module.exports = (ToolbarAndroidToExport: Class<
  NativeComponent<ToolbarAndroidProps>,
>);