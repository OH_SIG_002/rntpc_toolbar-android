
/**
 * This code was generated by [react-native-codegen](https://www.npmjs.com/package/react-native-codegen).
 *
 * Do not edit this file as changes may cause incorrect behavior and will be lost
 * once the code is regenerated.
 *
 * @generated by codegen project: GeneratePropsH.js
 */
#pragma once

#include <react/renderer/components/view/ViewProps.h>
#include <react/renderer/core/PropsParserContext.h>
#include <react/renderer/imagemanager/primitives.h>

namespace facebook {
namespace react {

class ToolbarAndroidProps final : public ViewProps {
 public:
  ToolbarAndroidProps() = default;
  ToolbarAndroidProps(const PropsParserContext& context, const ToolbarAndroidProps &sourceProps, const RawProps &rawProps);

#pragma mark - Props

  folly::dynamic nativeActions{};
  folly::dynamic actions{};
  ImageSource logo{};
  ImageSource navIcon{};
  ImageSource overflowIcon{};
  std::string subtitle{};
  std::string subtitleColor{};
  std::string title{};
  std::string titleColor{};
  int contentInsetStart{0};
  int contentInsetEnd{0};
  bool rtl{false};
  std::string testID{};
};

} // namespace react
} // namespace facebook
