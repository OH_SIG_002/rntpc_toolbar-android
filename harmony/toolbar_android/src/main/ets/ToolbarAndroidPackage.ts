/*
 * Copyright (c) 2024 Huawei Device Co., Ltd. All rights reserved
 * Use of this source code is governed by a MIT license that can be
 * found in the LICENSE file.
 */

import {
  RNPackage,
  DescriptorWrapperFactoryByDescriptorType,
  DescriptorWrapperFactoryByDescriptorTypeCtx,
} from '@rnoh/react-native-openharmony/ts';
import { RNC } from './generated/ts'

export class ToolbarAndroidPackage extends RNPackage {

  createDescriptorWrapperFactoryByDescriptorType(ctx: DescriptorWrapperFactoryByDescriptorTypeCtx): DescriptorWrapperFactoryByDescriptorType{
    const result: DescriptorWrapperFactoryByDescriptorType = {}
    result[RNC.ToolbarAndroid.NAME] = (ctx) => new RNC.ToolbarAndroid.DescriptorWrapper(ctx.descriptor)
    return result
  }
}